from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponsePermanentRedirect, HttpResponse

# Enter your name here
mhs_name = 'Zaki Raihan' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 5, 14) #TODO Implement this, format (Year, Month, Date)
hobby = 'Playing games'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'hobby' : hobby}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
